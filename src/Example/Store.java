package Example;

/**
 * Store is store which have number of items.
 * @author Wanchanapon Thanwaranurak and Voraton Lertrattanapaisal
 *
 */
public class Store {
	
	private int numberOfItem;
	
	/**
	 * Constructor for setup numberOfItem.
	 * @param numberOfItem is number of item that will be setup.
	 */
	public Store (int numberOfItem){
		
		this.numberOfItem = numberOfItem;
		
	}
	
	/**
	 * Increasing numberOfItem. 
	 */
	public void keep(){
		
		numberOfItem+=1;
		
	}
	
	/**
	 * Decreasing numberOfItem.
	 */
	public void get(){
		
		if (numberOfItem>0) numberOfItem-=1;
	
	}
}
