package Example;

/**
 * Get command is use for reducing number of thing in store.
 * @author Wanchanapon Thanwaranurak and Voraton Lertrattanapaisal
 *
 */
public class GetCommand implements Command {

	private Store store;
	
	/**
	 * Constructor for setup the store.
	 * @param store is store that will be setup.
	 */
	public GetCommand(Store store) {
		
		this.store =store;
		
	}
	
	/**
	 * Doing get method of store.
	 */
	public void execute() {
		
		store.get();
		
	}


}
