package Example;
/**
 * Command is interface which has execute() method.
 * @author Wanchanapon Thanwaranurak and Voraton Lertrattanapaisal
 *
 */
public interface Command {
	
	/**
	 * Execute the action.
	 */
	void execute();
}
