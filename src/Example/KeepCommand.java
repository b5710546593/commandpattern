package Example;

/**
 * Keep command is use for increasing number of thing in store.
 * @author Wanchanapon Thanwaranurak and Voraton Lertrattanapaisal
 *
 */
public class KeepCommand implements Command {

	private Store store;
	
	/**
	 * Constructor for setup the store.
	 * @param store is store that will be setup.
	 */
	public KeepCommand(Store store) {
		this.store =store;
	}
	
	/**
	 * Doing keep method of store.
	 */
	public void execute() {
		
		store.keep();
		
	}

}
