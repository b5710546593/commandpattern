package Example;

/**
 * Manager is invoker which manage the store.
 * @author Wanchanapon Thanwaranurak and Voraton Lertrattanapaisal
 *
 */
public class Manager {
	
	/**
	 * Execute the command.
	 * @param cmd is command that will be executed.
	 */
	public void manage(Command cmd){
		cmd.execute();
	}
	
}
