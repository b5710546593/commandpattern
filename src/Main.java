
public class Main {

	public static void main(String[] args) {
		
		Receiver receiver = new Receiver();
		Action action = new Action(receiver);
		Invoker invoker = new Invoker();
		invoker.executeCommand(action);
		
	}

}
