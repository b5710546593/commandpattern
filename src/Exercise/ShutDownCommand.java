package Exercise;
/**
 * Shutdown Command is command for shutting down computer.
 * @author Wanchanapon Thanwaranurak and Voraton Lertrattanapaisal
 *
 */
public class ShutDownCommand implements Command {

	   private Computer computer;
	   
	   /**
	    * Constructor for setup the computer that will use the command.
	    * @param computer that will be setup.
	    */
	   public ShutDownCommand(Computer computer) {
	      this.computer = computer;
	   }
	 
	   /**
	    * Shutdown Computer.
	    */
	   public void execute(){
	      computer.shutDown();
	   }

}
