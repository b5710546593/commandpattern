package Exercise;
/**
 * Computer have two action.
 * @author Wanchanapon Thanwaranurak and Voraton Lertrattanapaisal
 *
 */
class Computer {
	 
	   /**
	   * Shutting down the computer.
	   */
	   public void shutDown() {
	      System.out.println("computer is shut down");
	   }
	 
	   /**
	    * Restarting the computer.
	    */
	   public void restart() {
	      System.out.println("computer is restarted");
	   }
	   
}
