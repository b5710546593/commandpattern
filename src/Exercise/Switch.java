package Exercise;
/**
 * Switch is using for shutdown or restart the computer.
 * @author Wanchanapon Thanwaranurak and Voraton Lertrattanapaisal
 *
 */
class Switch {
	
	/**
	 * Execute the command.
	 * @param command will be executed.
	 */
	   public void action(Command command) {
	      command.execute();        
	   }
}
