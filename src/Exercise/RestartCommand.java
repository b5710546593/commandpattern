package Exercise;
/**
 * Restart Command is command for restarting computer.
 * @author Wanchanapon Thanwaranurak and Voraton Lertrattanapaisal
 *
 */
public class RestartCommand implements Command {

	   private Computer computer;
	   
	   /**
	    * Constructor for setup the computer that will use the command.
	    * @param computer that will be setup.
	    */
	   public RestartCommand(Computer computer) {
	      this.computer = computer;
	   }
	   
	   /**
	    * Restart Computer.
	    */
	   public void execute(){
	      computer.restart();;
	   }
}
