/**
 * Invoker is using for execute command.
 * @author Wanchanapon Thanwaranurak and Voraton Lertrattanapaisal
 *
 */
public class Invoker {
	/**
	 * Execute command.
	 * @param cmd is command that will be executed.
	 */
	public void executeCommand(Command cmd){
		cmd.execute();
	}
}
