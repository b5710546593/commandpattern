/**
 * Action is command to do something.
 * @author Wanchanapon Thanwaranurak and Voraton Lertrattanapaisal
 *
 */
public class Action implements Command {
	private Receiver receiver;
	/**
	 * Constructor for setup reciever.
	 * @param receiver is setup.
	 */
	public Action(Receiver receiver){
		this.receiver= receiver;
	}
	
	/**
	 * dosomething.
	 */
	public void execute() {
		receiver.doSomething();
	}

}
