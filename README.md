# Command Pattern #
## What is it ? ##
The pattern which is received command from invoker and send command to receiver to tell the receiver to do something following the command.

#![command.jpg](https://bitbucket.org/repo/GEKGn6/images/728666732-command.jpg)#
![ddddddddd.png](https://bitbucket.org/repo/GEKGn6/images/740852100-ddddddddd.png)
## How to Use ##
![CommandStructor.png](https://bitbucket.org/repo/GEKGn6/images/3009373422-CommandStructor.png)
1.) Write an Command interface which has method execute() for do the action.
```
#!java

public interface Command {
	void execute();
}
```
2.) Write action class that implements Command.

```
#!java

public class Action implements Command {

	public void execute() {
		//do some action.
	}

}
```
3.) Let Receiver class has method that receive command in parameter and execute to do something.

```
#!java


public class Receiver {
	
	public void doSomething(Command cmd){
		cmd.execute();
	}
	
}
```
4.) Main

```
#!java

public class Main {

	public static void main(String[] args) {
		
		Receiver receiver = new Receiver();
		Action action = new Action();
		receiver.doSomething(action);
		
	}

}
```

### Example ###
![Example.png](https://bitbucket.org/repo/GEKGn6/images/2919838051-Example.png)

```
#!java

public class Store {
	
	private int numberOfItem;
	
	public Store (int numberOfItem){
		
		this.numberOfItem = numberOfItem;
		
	}
	
	public void keep(){
		
		numberOfItem+=1;
		
	}
	
	public void get(){
		
		if (numberOfItem>0) numberOfItem-=1;
	
	}
}

public interface Command {
	void execute();
}

public class GetCommand implements Command {

	private Store store;
	
	public GetCommand(Store store) {
		
		this.store =store;
		
	}
	
	public void execute() {
		
		store.get();
		
	}


}

public class KeepCommand implements Command {

	private Store store;
	
	public KeepCommand(Store store) {
		this.store =store;
	}
	
	public void execute() {
		
		store.keep();
		
	}

}

public class Manager {
	
	public void manage(Command cmd){
		cmd.execute();
	}
	
}

```
[Source Code](https://bitbucket.org/b5710546593/commandpattern/src/698d235e8fb91f890a4e594b035f0dd90543bcfb/src/Example/?at=master)

### Exercise ###
Apply Command pattern to the classes below. (Invoker is Switch)

```
#!java

 class Computer {
 
   public void shutDown() {
      System.out.println("computer is shut down");
   }
 
   public void restart() {
      System.out.println("computer is restarted");
   }
}
```
[Solution Source Code](https://bitbucket.org/b5710546593/commandpattern/src/698d235e8fb91f890a4e594b035f0dd90543bcfb/src/Exercise/?at=master)